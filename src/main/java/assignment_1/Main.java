package assignment_1;

import org.apache.hadoop.conf.Configuration;
import org.apache.kerby.config.Conf;

public class Main {
    public static void main(String[] args) throws Exception {
        //----------Create 100 csv Files
        CsvFileGeneration csv=new CsvFileGeneration();
        csv.GenerateCSVs();

        //----------Upload Files from Local to HDFS
        LocalToHDFS lth=new LocalToHDFS();
        lth.copyFromLocal();

        //-----------Read Files from HDFS
        HBaseUtils hbase=new HBaseUtils();
        hbase.insertCSVDataToHbase(String, Configuration);
    }
}
