package assignment_3;

import protoPackage.BuildingProto;
import protoPackage.EmployeeProto;
import java.io.*;

public class ProtoBfUtilities {

    private final String splitWith = ",";
    private static BufferedReader csvReader = null;
    private static FileOutputStream fos = null;

    public ProtoBfUtilities(String csvFilePath, String serializedFileOutputPath) throws FileNotFoundException {
        csvReader = new BufferedReader(new FileReader(csvFilePath));
        fos = new FileOutputStream(serializedFileOutputPath);
    }

    public void serializeEmployeeCSV() throws IOException {
        String record;
        while ((record = csvReader.readLine()) != null) {
            System.out.println(record);
            String[] employeeRecord = record.split(splitWith);
            EmployeeProto.EmployeeDetails employeeProtoObj = EmployeeProto.EmployeeDetails.newBuilder().setEmployeeId(employeeRecord[0])
                    .setName(employeeRecord[1]).setBuildingCode(employeeRecord[2])
                    .setFloorNo(getFloorEnum(employeeRecord[3]))
                    .setSalary(Integer.parseInt(employeeRecord[4]))
                    .setDepartment(employeeRecord[5])
                    .build();
            EmployeeProto.EmployeeDatabase empDB = EmployeeProto.EmployeeDatabase.newBuilder().addEmployee(employeeProtoObj).build();
            empDB.writeTo(fos);
        }
        System.out.println("File Serialized");
    }


    public void serializeBuildingCSV() throws IOException {
        String record;
        while ((record = csvReader.readLine()) != null) {
            System.out.println(record);
            String[] buildingRecord = record.split(splitWith);
            BuildingProto.BuildingDetails buildingProtoObj = BuildingProto.BuildingDetails.newBuilder()
                    .setBuildingCode(buildingRecord[0])
                    .setTotalFloors(Integer.parseInt(buildingRecord[1]))
                    .setCompaniesInTheBuilding(buildingRecord[2])
                    .setCafteriaCode(buildingRecord[3])
                    .build();
            BuildingProto.BuildingDatabase buildingDB = BuildingProto.BuildingDatabase.newBuilder().addBuilding(buildingProtoObj).build();
            buildingDB.writeTo(fos);
        }
        System.out.println("File Serialized");
    }

    private EmployeeProto.EmployeeDetails.Floor getFloorEnum(String floorNo) {
        EmployeeProto.EmployeeDetails.Floor floorValue = EmployeeProto.EmployeeDetails.Floor.First;
        switch (floorNo) {
            case "First":
                floorValue = EmployeeProto.EmployeeDetails.Floor.First;
                break;
            case "Second":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Second;
                break;
            case "Third":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Third;
                break;
            case "Fourth":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Fourth;
                break;
            case "Fifth":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Fifth;
                break;
        }
        return floorValue;
    }
}

